import sys
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5 import QtGui
from spiderMain import Ui_Form
from bs4 import BeautifulSoup
import urllib.request
import nltk
from nltk.corpus import stopwords



class AppWindow(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.show()
        self.setWindowIcon(QtGui.QIcon('spider.svg'))

        self.ui.pushButton.clicked.connect(self.crawlbtn)
        self.ui.pushButton_3.clicked.connect(self.exfp)

    def crawlbtn(self):
        self.ui.label_2.setText("Status: Crawling")
        response = urllib.request.urlopen(self.ui.lineEdit.text())

        html = response.read()
        soup = BeautifulSoup(html,"html5lib")
        text = soup.get_text(strip=True)
        tokens = [t for t in text.split()]
        clean_tokens = tokens[:]
        sr = stopwords.words('english')
        for token in tokens:
            if token in stopwords.words('english'):
                clean_tokens.remove(token)
        freq = nltk.FreqDist(clean_tokens)
        for key,val in freq.items():
            print (str(key) + ':' + str(val))

        freq.plot(20,cumulative=False)

    def exfp(self):
        sys.exit()

        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = AppWindow()
    w.show()
    sys.exit(app.exec_())